package com.softreactor.template.domain.account;

import java.util.List;

/**
 * Role domain repository interface.
 */
public interface RoleRepository {

	/**
	 * Creates new role object.
	 */
	Role createObject();

	/**
	 * Finds all roles.
	 */
	List<? extends Role> findAll();

	/**
	 * Saves role object.
	 */
	void save(Role role);

	/**
	 * Removes role by name.
	 */
	void removeByName(String name);

}
