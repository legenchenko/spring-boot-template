package com.softreactor.template.domain.account;

import java.util.UUID;

/**
 * User entity interface.
 */
public interface User {
	UUID getId();

	void setId(UUID id);

	String getEmail();

	void setEmail(String email);

	String getPassword();

	void setPassword(String password);
}
