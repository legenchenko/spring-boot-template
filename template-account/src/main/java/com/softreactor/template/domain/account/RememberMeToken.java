package com.softreactor.template.domain.account;

import java.util.Date;

/**
 * Remember me token entity interface.
 */
public interface RememberMeToken {

	byte[] getId();

	void setId(byte[] id);

	String getUsername();

	void setUsername(String username);

	byte[] getToken();

	void setToken(byte[] token);

	Date getDate();

	void setDate(Date date);
}
