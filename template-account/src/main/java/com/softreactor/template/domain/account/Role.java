package com.softreactor.template.domain.account;

/**
 * Role entity interface.
 */
public interface Role {
	String getName();

	void setName(String name);
}
