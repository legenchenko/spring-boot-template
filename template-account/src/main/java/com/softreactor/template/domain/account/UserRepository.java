package com.softreactor.template.domain.account;

import java.util.List;
import java.util.UUID;

/**
 * User repository interface.
 */
public interface UserRepository {

	/**
	 * Creates new user object.
	 */
	User createObject();

	/**
	 * Saves user object.
	 */
	void save(User user);

	/**
	 * Finds user by id.
	 */
	User findById(UUID userId);

	/**
	 * Finds user by email.
	 */
	User findByEmail(String email);

	/**
	 * Removes user by id.
	 */
	void removeById(UUID userId);

	/**
	 * Adds role to user.
	 */
	void addRole(UUID userId, String role);

	/**
	 * Finds user roles by id.
	 */
	List<String> findRolesById(UUID userId);

	/**
	 * Removes role from user.
	 */
	void removeRole(UUID userId, String role);

}
