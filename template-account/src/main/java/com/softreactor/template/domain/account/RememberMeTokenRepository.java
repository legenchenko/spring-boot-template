package com.softreactor.template.domain.account;

import java.util.Date;

/**
 * Remember me token repository interface.
 */
public interface RememberMeTokenRepository {
	/**
	 * Creates new token object.
	 */
	RememberMeToken createObject();

	/**
	 * Finds token by id.
	 */
	RememberMeToken findById(byte[] id);

	/**
	 * Saves token object.
	 */
	void save(RememberMeToken token);

	/**
	 * Updates token record by id.
	 */
	void updateToken(byte[] id, byte[] tokenValue, Date lastUsed);

	/**
	 * Removes tokens by username.
	 */
	void removeByUsername(String username);
}
