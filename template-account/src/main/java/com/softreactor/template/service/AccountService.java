package com.softreactor.template.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import com.softreactor.template.domain.account.RememberMeToken;
import com.softreactor.template.domain.account.RememberMeTokenRepository;
import com.softreactor.template.domain.account.Role;
import com.softreactor.template.domain.account.RoleRepository;
import com.softreactor.template.domain.account.User;
import com.softreactor.template.domain.account.UserRepository;

/**
 * Account service class.
 */
@Service
@Validated
public class AccountService implements PersistentTokenRepository {
	private final UserRepository userRepository;
	private final RoleRepository roleRepository;
	private final RememberMeTokenRepository rememberMeTokenRepository;
	private final StandardPasswordEncoder passwordEncoder;

	/**
	 * Class constructor.
	 */
	@Autowired
	public AccountService(
			@Value("#{configProperties}") Properties configProperties,
			UserRepository userRepository,
			RoleRepository roleRepository,
			RememberMeTokenRepository rememberMeTokenRepository) {
		Assert.notNull(configProperties, "Properties must not be null");
		Assert.notNull(userRepository, "UserRepository must not be null");
		Assert.notNull(roleRepository, "RoleRepository must not be null");
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.rememberMeTokenRepository = rememberMeTokenRepository;
		this.passwordEncoder = new StandardPasswordEncoder(configProperties.getProperty("salt.secret"));
	}

	/**
	 * Creates new user.
	 */
	@Transactional
	public User createUser(@NotBlank @Email String email, @NotBlank String password) {
		User user = userRepository.createObject();
		user.setEmail(email.trim());
		user.setPassword(encodePassword(password));
		createUser(user, Arrays.asList("user"));
		return user;
	}

	/**
	 * Finds user by id.
	 */
	public User findUserById(@NotNull UUID userId) {
		return userRepository.findById(userId);
	}

	/**
	 * Finds user by email.
	 */
	public User findUserByEmail(@NotBlank @Email String email) {
		return userRepository.findByEmail(email.trim());
	}

	/**
	 * Removes user by id.
	 */
	@Transactional
	public void removeUser(@NotNull UUID userId) {
		User user = userRepository.findById(userId);
		if (user == null) {
			return;
		}

		rememberMeTokenRepository.removeByUsername(user.getEmail());
		userRepository.removeById(userId);
	}

	/**
	 * Checks user credentials.
	 */
	public boolean checkCredentials(@NotNull User user, @NotBlank String presentedPassword) {
		return passwordEncoder.matches(presentedPassword.trim(), user.getPassword());
	}


	/**
	 * Finds all roles.
	 */
	@Transactional
	public List<String> findAllRoles() {
		return roleRepository.findAll().stream().map(r -> r.getName()).collect(Collectors.toList());
	}

	/**
	 * Creates new role.
	 */
	@Transactional
	public Role createRole(@NotBlank String name) {
		Role role = roleRepository.createObject();
		role.setName(name);
		roleRepository.save(role);
		return role;
	}

	/**
	 * Removes role.
	 */
	@Transactional
	public void removeRole(@NotBlank String name) {
		roleRepository.removeByName(name);
	}

	/**
	 * Adds role to user.
	 */
	@Transactional
	public void addUserRole(@NotNull UUID userId, @NotBlank String role) {
		userRepository.addRole(userId, role.trim());
	}

	/**
	 * Finds user roles by id.
	 */
	public List<String> findUserRoles(UUID userId) {
		return userRepository.findRolesById(userId);
	}

	/**
	 * Removes role from user.
	 */
	@Transactional
	public void removeUserRole(UUID userId, String role) {
		userRepository.removeRole(userId, role);
	}

	/**
	 * @see PersistentTokenRepository#createNewToken(PersistentRememberMeToken)
	 */
	@Override
	@Transactional
	public void createNewToken(PersistentRememberMeToken token) {
		Assert.notNull(token, "Token must not be null");
		RememberMeToken tokenObj = rememberMeTokenRepository.createObject();
		tokenObj.setId(Base64.decode(token.getSeries().getBytes()));
		tokenObj.setUsername(token.getUsername());
		tokenObj.setToken(Base64.decode(token.getTokenValue().getBytes()));
		tokenObj.setDate(token.getDate());
		rememberMeTokenRepository.save(tokenObj);
	}

	/**
	 * @see PersistentTokenRepository#updateToken(String, String, Date)
	 */
	@Override
	@Transactional
	public void updateToken(String series, String tokenValue, Date lastUsed) {
		Assert.notNull(series, "Series must not be null");
		Assert.notNull(tokenValue, "TokenValue must not be null");
		Assert.notNull(lastUsed, "LastUsed must not be null");
		rememberMeTokenRepository.updateToken(Base64.decode(series.getBytes()),
				Base64.decode(tokenValue.getBytes()),
				lastUsed);
	}

	/**
	 * @see PersistentTokenRepository#getTokenForSeries(String)
	 */
	@Override
	public PersistentRememberMeToken getTokenForSeries(String seriesId) {
		Assert.notNull(seriesId, "SeriesId must not be null");
		RememberMeToken tokenObj = rememberMeTokenRepository.findById(Base64.decode(seriesId.getBytes()));
		if (tokenObj == null) {
			return null;
		}

		return new PersistentRememberMeToken(tokenObj.getUsername(),
				new String(Base64.encode(tokenObj.getId())),
				new String(Base64.encode(tokenObj.getToken())),
				tokenObj.getDate());
	}

	/**
	 * @see PersistentTokenRepository#removeUserTokens(String)
	 */
	@Override
	@Transactional
	public void removeUserTokens(String username) {
		Assert.notNull(username, "Username must not be null");
		rememberMeTokenRepository.removeByUsername(username);
	}

	/**
	 * Creates user with roles.
	 */
	private void createUser(User user, Collection<String> roles ) {
		userRepository.save(user);
		if (roles != null) {
			final UUID userId = user.getId();
			roles.forEach(role -> userRepository.addRole(userId, role));
		}
	}

	/**
	 * Encodes password using passwordEncoder.
	 */
	private String encodePassword(String password) {
		return passwordEncoder.encode(password.trim());
	}
}
