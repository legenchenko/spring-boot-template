package com.softreactor.template.util;

import java.nio.ByteBuffer;
import java.util.UUID;

import org.springframework.security.crypto.codec.Hex;
import org.springframework.util.Assert;

/**
 * Domain related utility methods.
 */
public class DomainUtils {

	/**
	 * Generates new entity id.
	 */
	public static UUID generateId() {
		return UUID.randomUUID();
	}

	/**
	 * Converts UUID to bytes array.
	 */
	public static byte[] toBytes(UUID uuid) {
		Assert.notNull(uuid, "UUID must not be null");
		ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
		bb.putLong(uuid.getMostSignificantBits());
		bb.putLong(uuid.getLeastSignificantBits());
		return bb.array();
	}

	/**
	 * Converts bytes array to hexadecimal string.
	 */
	public static String bytesToHex(byte[] bytes) {
		if (bytes == null) {
			return null;
		}

		return new String(Hex.encode(bytes));
	}

	/**
	 * Converts hexadecimal string to bytes array.
	 */
	public static byte[] hexToBytes(String str) {
		if (str == null) {
			return null;
		}

		return Hex.decode(str);
	}

}
