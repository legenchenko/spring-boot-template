package com.softreactor.template.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.test.context.junit4.SpringRunner;

import com.softreactor.template.domain.account.User;

/**
 * Test suite for account service.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {
	private final static String EMAIL = "test@email.com";
	private final static String PASSWORD = "testPassword";

	@Autowired
	private AccountService accountService;

	@Test
	public void testUser() {
		User accountUser = accountService.createUser(EMAIL, PASSWORD);
		assertNotNull("Create user", accountUser);

		List<String> roles = accountService.findUserRoles(accountUser.getId());
		assertTrue("Find \"user\" role in created user", roles.contains("user"));

		boolean exception = false;
		try {
			accountService.createUser(EMAIL, "testPassword2");
		}
		catch (DataIntegrityViolationException ex) {
			exception = true;
		}

		assertTrue("Create user with same email", exception);

		assertEquals("Find user by id.", accountUser, accountService.findUserById(accountUser.getId()));
		assertEquals("Find user by email.", accountUser, accountService.findUserByEmail(EMAIL));
		assertNotNull("Create role", accountService.createRole("test"));

		exception = false;
		try {
			accountService.createRole("test");
		}
		catch (DataIntegrityViolationException ex) {
			exception = true;
		}

		assertTrue("Create role with same name", exception);

		roles = accountService.findAllRoles();
		assertTrue("Find added role", roles.contains("test"));

		accountService.addUserRole(accountUser.getId(), "test");
		roles = accountService.findUserRoles(accountUser.getId());
		assertTrue("Find role added to user", roles.contains("test"));

		accountService.removeUserRole(accountUser.getId(), "test");
		roles = accountService.findUserRoles(accountUser.getId());
		assertFalse("Find role removed from user", roles.contains("test"));

		accountService.removeRole("test");
		roles = accountService.findAllRoles();
		assertFalse("Find removed role", roles.contains("test"));

		accountService.removeUser(accountUser.getId());
		assertNull("Find removed user by id.", accountService.findUserById(accountUser.getId()));
		assertNull("Find removed user by email.", accountService.findUserByEmail(accountUser.getEmail()));
	}

	@Test
	public void testRememberMe() {
		SecureRandom random = new SecureRandom();
		byte[] bytes = new byte[16];
		random.nextBytes(bytes);
		String series = new String(Base64.encode(bytes));

		random.nextBytes(bytes);
		String tokenValue = new String(Base64.encode(bytes));

		Date date = new Date(1000 * (System.currentTimeMillis() / 1000));
		PersistentRememberMeToken token = new PersistentRememberMeToken(EMAIL, series, tokenValue, date);
		accountService.createNewToken(token);

		PersistentRememberMeToken loadedToken = accountService.getTokenForSeries(series);
		assertNotNull("Load token by id", loadedToken);
		assertTrue("Check token data", series.equals(loadedToken.getSeries())
				&& EMAIL.equals(loadedToken.getUsername())
				&& tokenValue.equals(loadedToken.getTokenValue())
				&& date.equals(loadedToken.getDate()));

		random.nextBytes(bytes);
		String newTokenValue = new String(Base64.encode(bytes));
		Date lastUsed = new Date(1000 * (System.currentTimeMillis() / 1000) + 1000);
		accountService.updateToken(series, newTokenValue, lastUsed);

		loadedToken = accountService.getTokenForSeries(series);
		assertNotNull("Load updated token by id", loadedToken);
		assertTrue("Check updated token data", newTokenValue.equals(loadedToken.getTokenValue())
				&& lastUsed.equals(loadedToken.getDate()));

		accountService.removeUserTokens(EMAIL);
		assertNull("Load removed token by id", accountService.getTokenForSeries(series));
	}

}
