package com.softreactor.template.web;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.softreactor.template.domain.account.User;
import com.softreactor.template.service.AccountService;

/**
 * Test REST controller.
 */
@RestController
public class TestRestController {

	protected final Log logger = LogFactory.getLog(getClass());

	private final MessageSource messageSource;
	private final AccountService accountService;

	@Autowired
	public TestRestController(MessageSource messageSource, AccountService accountService) {
		Assert.notNull(messageSource, "MessageSource must not be null");
		Assert.notNull(accountService, "AccountService must not be null");
		this.messageSource = messageSource;
		this.accountService = accountService;
	}

	@RequestMapping("/hello")
	public String hello(@RequestParam(value = "name", required = false, defaultValue = "Guest") String name,
			Locale locale) {
		return messageSource.getMessage("home.welcome", new Object[] { name }, locale)
				+ "\nLocale: " + locale.toString();
	}

	@RequestMapping("/test")
	public List<?> test() {
		List<Object> result = new LinkedList<>();

		try {
			accountService.createRole("admin");
			accountService.createRole("user");
		}
		catch (DataIntegrityViolationException e) {
		}

		User user = accountService.findUserByEmail("legenchenko@softreactor.com");
		try {
			if (user != null) {
				accountService.removeUser(user.getId());
			}

			user = accountService.createUser("legenchenko@softreactor.com", "test");
		}
		catch (DataIntegrityViolationException e) {
			e.printStackTrace();
		}

		if (user != null) {
			result.add(user);

			List<String> roles = accountService.findUserRoles(user.getId());
			if (roles != null) {
				result.add(roles);
			}
		}

		return result;
	}

}
