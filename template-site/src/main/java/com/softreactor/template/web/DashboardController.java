package com.softreactor.template.web;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * DashboardController class.
 * Contains actions for dashboard page.
 */
@Controller
@Secured("ROLE_USER")
public class DashboardController {

	@RequestMapping("/dashboard")
	public String dashboard() {
		return "dashboard";
	}
}
