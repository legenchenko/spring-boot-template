package com.softreactor.template.web;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * LoginForm class.
 */
public class LoginForm {

	@NotBlank
	@Email
	private String email;

	@NotBlank
	private String password;

	private boolean rememberMe;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(boolean rememeberMe) {
		this.rememberMe = rememeberMe;
	}

}
