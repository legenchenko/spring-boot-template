package com.softreactor.template.web;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * HomeController class.
 * Contains home actions.
 */
@Controller
public class HomeController {

	/**
	 * Home action.
	 */
	@RequestMapping(value = "/", method = { RequestMethod.GET, RequestMethod.POST })
	public String home(Authentication auth, @RequestParam(value = "name", required = false, defaultValue = "World") String name,
			Model model) {
		if (auth != null) {
			return "forward:/dashboard";
		}

		model.addAttribute("name", name);
		return "home";
	}

	/**
	 * Contact action.
	 */
	@GetMapping("/contact")
	public String contact() {
		return "contact";
	}

}
