package com.softreactor.template;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Spring boot application class.
 */
@SpringBootApplication
@ComponentScan(basePackages = {
		"com.softreactor.template.config",
		"com.softreactor.template.service",
		"com.softreactor.template.web"
})
public class SpringBootTemplateApplication {

	/**
	 * Application main method.
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringBootTemplateApplication.class, args);
	}

	/**
	 * Configuration properties bean creator.
	 */
	@Bean
	public Properties configProperties() throws IOException {
		Properties properties = new Properties();
		try (InputStream stream = this.getClass().getResourceAsStream("/config.properties")) {
			properties.load(stream);
		}

		return properties;
	}

}
