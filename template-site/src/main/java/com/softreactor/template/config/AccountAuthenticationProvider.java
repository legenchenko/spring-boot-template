package com.softreactor.template.config;

import java.util.List;

import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.softreactor.template.domain.account.User;
import com.softreactor.template.service.AccountService;

/**
 * AccountAuthenticationProvider class.
 * Implements authentication provider based on account service.
 */
public class AccountAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	private final AccountService accountService;

	/**
	 * Class constructor.
	 */
	public AccountAuthenticationProvider(AccountService accountService) {
		Assert.notNull(accountService, "AccountService must not be null");
		this.accountService = accountService;
	}

	/**
	 * Creates UserDetails instance by user object and roles.
	 */
	public static UserDetails createUserDetails(User user, List<String> roles) {
		return new AccountUserDetails(user, roles);
	}

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		Object credentials = authentication.getCredentials();
		if (credentials == null) {
			throwException();
		}

		String presentedPassword = credentials.toString();
		if (!accountService.checkCredentials(((AccountUserDetails) userDetails).getUser(), presentedPassword)) {
			throwException();
		}
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		User user = accountService.findUserByEmail(username);
		if (user == null) {
			throwException();
		}

		return createUserDetails(user, accountService.findUserRoles(user.getId()));
	}

	private void throwException() throws AuthenticationException {
		throw new InternalAuthenticationServiceException("error.incorrectLoginOrPassword");
	}

}
