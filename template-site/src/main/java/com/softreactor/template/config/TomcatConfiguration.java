package com.softreactor.template.config;

import java.util.Properties;

import javax.servlet.Servlet;

import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;
import org.apache.coyote.AbstractProtocol;
import org.apache.coyote.ProtocolHandler;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.embedded.Compression;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * Tomcat configuration class.
 * Adds additional connector to handle non-SSL HTTP connector.
 */
@Configuration
@ConditionalOnClass({ Servlet.class, Tomcat.class })
public class TomcatConfiguration {

	private final Integer port;

	@Autowired
	public TomcatConfiguration(@Value("#{configProperties}") Properties configProperties) {
		Assert.notNull(configProperties, "Properties must not be null");
		port = Integer.parseInt(configProperties.getProperty("server.port"));
	}

	@Bean
	public Integer port() {
		return port;
	}

	@Bean
	public TomcatEmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory() {
			@Override
			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(Tomcat tomcat) {
				tomcat.getService().addConnector(createStandardConnector());
				return super.getTomcatEmbeddedServletContainer(tomcat);
			}

			private Connector createStandardConnector() {
				Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
				connector.setPort(port());

				if (StringUtils.hasText(this.getServerHeader())) {
					connector.setAttribute("server", this.getServerHeader());
				}

				if (connector.getProtocolHandler() instanceof AbstractProtocol) {
					if (getAddress() != null) {
						((AbstractProtocol<?>) connector.getProtocolHandler()).setAddress(getAddress());
					}
				}

				if (getUriEncoding() != null) {
					connector.setURIEncoding(getUriEncoding().name());
				}

				// If ApplicationContext is slow to start we want Tomcat not to bind to the socket
				// prematurely...
				connector.setProperty("bindOnInit", "false");

				if (getCompression() != null && getCompression().getEnabled()) {
					ProtocolHandler handler = connector.getProtocolHandler();
					if (handler instanceof AbstractHttp11Protocol) {
						AbstractHttp11Protocol<?> protocol = (AbstractHttp11Protocol<?>) handler;
						Compression compression = getCompression();
						protocol.setCompression("on");
						protocol.setCompressionMinSize(compression.getMinResponseSize());
						protocol.setCompressableMimeType(
								StringUtils.arrayToCommaDelimitedString(compression.getMimeTypes()));
						if (getCompression().getExcludedUserAgents() != null) {
							protocol.setNoCompressionUserAgents(
									StringUtils.arrayToCommaDelimitedString(
											getCompression().getExcludedUserAgents()));
						}
					}
				}

				for (TomcatConnectorCustomizer customizer : getTomcatConnectorCustomizers()) {
					customizer.customize(connector);
				}

				return connector;
			}
		};

		return factory;
	}

}
