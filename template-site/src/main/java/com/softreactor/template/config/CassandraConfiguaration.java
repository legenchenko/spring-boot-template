package com.softreactor.template.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Cassandra configuration class.
 */
@Configuration
@ConditionalOnClass(name = "com.datastax.driver.core.Cluster")
@ComponentScan("com.softreactor.template.domain.*.cassandra")
public class CassandraConfiguaration {

}
