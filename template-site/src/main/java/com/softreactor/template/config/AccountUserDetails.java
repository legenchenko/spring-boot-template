package com.softreactor.template.config;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.softreactor.template.domain.account.User;

/**
 * AccountUserDetails class.
 * Implements user details based on domain user.
 */
@SuppressWarnings("serial")
public class AccountUserDetails implements UserDetails, CredentialsContainer {

	private final User user;
	private final Set<GrantedAuthority> authorities;

	/**
	 * Class constructor.
	 */
	public AccountUserDetails(User user, List<String> roles) {
		Assert.notNull(user, "User must not be null");
		Assert.notNull(roles, "Roles must not be null");
		this.user = user;
		this.authorities = Collections.unmodifiableSet(createAuthorities(roles));
	}

	@Override
	public void eraseCredentials() {
		user.setPassword(null);
	}

	/**
	 * Creates sorted GrantedAuthority set from roles list.
	 */
	private static SortedSet<GrantedAuthority> createAuthorities(List<String> roles) {
		SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<GrantedAuthority>((g1, g2) ->
		{
			if (g2.getAuthority() == null) {
				return -1;
			}

			if (g1.getAuthority() == null) {
				return 1;
			}

			return g1.getAuthority().compareTo(g2.getAuthority());
		});

		sortedAuthorities.addAll(roles.stream()
				.map(r -> new SimpleGrantedAuthority("ROLE_" + r.toUpperCase()))
				.collect(Collectors.toList()));
		return sortedAuthorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public User getUser() {
		return user;
	}

}
