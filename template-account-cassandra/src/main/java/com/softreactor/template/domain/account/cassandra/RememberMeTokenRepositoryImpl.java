package com.softreactor.template.domain.account.cassandra;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraBatchOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.datastax.driver.core.querybuilder.Batch;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.softreactor.template.domain.account.RememberMeToken;
import com.softreactor.template.domain.account.RememberMeTokenRepository;
import com.softreactor.template.util.DomainUtils;

/**
 * UserRepositoryImpl class.
 * Implements RememberMeTokenRepository for Cassandra.
 */
@Component
public class RememberMeTokenRepositoryImpl implements RememberMeTokenRepository {
	private final CassandraTemplate template;

	/**
	 * Class constructor.
	 */
	@Autowired
	public RememberMeTokenRepositoryImpl(CassandraTemplate template) {
		Assert.notNull(template, "CassandraOperations must not be null");
		this.template = template;
	}

	@Override
	public RememberMeToken createObject() {
		return new RememberMeTokenEntity();
	}

	@Override
	public RememberMeToken findById(byte[] id) {
		return template.selectOneById(RememberMeTokenEntity.class, DomainUtils.bytesToHex(id));
	}

	@Override
	public void save(RememberMeToken token) {
		RememberMeTokenByUserameEntity tokenByUsername = new RememberMeTokenByUserameEntity();
		tokenByUsername.setUsername(token.getUsername());
		tokenByUsername.setTokenId(token.getId());

		CassandraBatchOperations batchOps = template.batchOps();
		batchOps.insert(tokenByUsername);
		batchOps.insert(token);
		batchOps.execute();
	}

	@Override
	public void updateToken(byte[] id, byte[] tokenValue, Date lastUsed) {
		template.execute(QueryBuilder.update("remember_me_token")
				.with(QueryBuilder.set("\"token\"", DomainUtils.bytesToHex(tokenValue)))
				.and(QueryBuilder.set("date", lastUsed))
				.where(QueryBuilder.eq("id", DomainUtils.bytesToHex(id))));
	}

	@Override
	public void removeByUsername(String username) {
		Select selectCql = QueryBuilder.select().from("remember_me_token_by_username");
		selectCql.where(QueryBuilder.eq("username", username));

		List<RememberMeTokenByUserameEntity> tokensByUsername =
				template.select(selectCql, RememberMeTokenByUserameEntity.class);

		Batch batch = QueryBuilder.batch();
		tokensByUsername.forEach(t -> {
			batch.add(QueryBuilder.delete().from("remember_me_token")
					.where(QueryBuilder.eq("id", DomainUtils.bytesToHex(t.getTokenId()))));
		});

		batch.add(QueryBuilder.delete().from("remember_me_token_by_username")
				.where(QueryBuilder.eq("username", username)));

		template.execute(batch);
	}

}
