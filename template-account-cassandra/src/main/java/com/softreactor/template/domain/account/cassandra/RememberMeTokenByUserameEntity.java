package com.softreactor.template.domain.account.cassandra;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

import com.softreactor.template.util.DomainUtils;

/**
 * Remember me token by username entity class for Cassandra.
 */
@Table("remember_me_token_by_username")
public class RememberMeTokenByUserameEntity {

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, ordinal = 1)
	private String username;

	@PrimaryKeyColumn(name = "token_id", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private String tokenId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public byte[] getTokenId() {
		return DomainUtils.hexToBytes(tokenId) ;
	}

	public void setTokenId(byte[] tokenId) {
		this.tokenId = DomainUtils.bytesToHex(tokenId);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tokenId == null) ? 0 : tokenId.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RememberMeTokenByUserameEntity other = (RememberMeTokenByUserameEntity) obj;
		if (tokenId == null) {
			if (other.tokenId != null)
				return false;
		} else if (!tokenId.equals(other.tokenId))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
