package com.softreactor.template.domain.account.cassandra;

import java.util.UUID;

import com.softreactor.template.domain.account.User;

/**
 * Base class for user entities.
 */
public abstract class BaseUserEntity implements User {

	private UUID id;
	private String email;
	private String password;

	/**
	 * Class constructor.
	 */
	public BaseUserEntity() {
	}

	/**
	 * Constructs object using source object data.
	 */
	public BaseUserEntity(BaseUserEntity source) {
		id = source.id;
		email = source.email;
		password = source.password;
	}

	@Override
	public UUID getId() {
		return id;
	}

	@Override
	public void setId(UUID id) {
		this.id = id;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof BaseUserEntity))
			return false;
		BaseUserEntity other = (BaseUserEntity) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

}
