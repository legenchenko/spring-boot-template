package com.softreactor.template.domain.account.cassandra;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

/**
 * User by email entity class for Cassandra.
 */
@Table("user_by_email")
public class UserByEmailEntity extends BaseUserEntity {
	/**
	 * Class constructor.
	 */
	public UserByEmailEntity() {
	}

	/**
	 * Constructs object using source object data.
	 */
	public UserByEmailEntity(BaseUserEntity source) {
		super(source);
	}

	@PrimaryKey
	@Override
	public String getEmail() {
		return super.getEmail();
	}

}
