package com.softreactor.template.domain.account.cassandra;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.cassandra.core.CassandraBatchOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.datastax.driver.core.querybuilder.Batch;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.softreactor.template.domain.account.Role;
import com.softreactor.template.domain.account.RoleRepository;
import com.softreactor.template.domain.account.User;
import com.softreactor.template.domain.account.UserRepository;
import com.softreactor.template.util.DomainUtils;

/**
 * UserRepositoryImpl class.
 * Implements UserRepository for Cassandra.
 */
@Component
public class UserRepositoryImpl implements UserRepository {

	private final CassandraTemplate template;
	private final RoleRepository roleRepository;

	/**
	 * Class constructor.
	 */
	@Autowired
	public UserRepositoryImpl(CassandraTemplate template, RoleRepository roleRepository) {
		Assert.notNull(template, "CassandraOperations must not be null");
		Assert.notNull(roleRepository, "RoleRepository must not be null");
		this.template = template;
		this.roleRepository = roleRepository;
	}

	@Override
	public User createObject() {
		return new UserEntity();
	}

	@Override
	public void save(User user) {
		CassandraBatchOperations batchOps;

		if (user.getId() == null) {
			checkEmailNotPresent(user.getEmail());

			user.setId(DomainUtils.generateId());
			batchOps = template.batchOps();
			batchOps.insert(new UserEntity((BaseUserEntity) user));
			batchOps.insert(new UserByEmailEntity((BaseUserEntity) user));
		}
		else {
			UserEntity savedUser = template.selectOneById(UserEntity.class, user.getId());
			if (savedUser == null) {
				throw new DataIntegrityViolationException("Invalid user id: " + user.getId());
			}

			final boolean emailChanged = user.getEmail().equals(savedUser.getEmail());
			if (emailChanged) {
				checkEmailNotPresent(user.getEmail());
			}

			batchOps = template.batchOps();
			batchOps.update(new UserEntity((BaseUserEntity) user));
			if (emailChanged) {
				batchOps.delete(UserByEmailEntity.class, user.getEmail());
				batchOps.insert(new UserByEmailEntity((BaseUserEntity) user));
			}
			else {
				batchOps.update(new UserByEmailEntity((BaseUserEntity) user));
			}
		}

		batchOps.execute();
	}

	@Override
	public User findById(UUID userId) {
		return template.selectOneById(UserEntity.class, userId);
	}

	@Override
	public User findByEmail(String email) {
		return template.selectOneById(UserByEmailEntity.class, email);
	}

	@Override
	public void removeById(UUID userId) {
		Batch batch = QueryBuilder.batch();

		UserEntity user = template.selectOneById(UserEntity.class, userId);
		if (user == null) {
			throw new DataIntegrityViolationException("Invalid user id: " + userId);
		}

		batch.add(QueryBuilder.delete().from("user_role").where(QueryBuilder.eq("user_id", userId)));
		batch.add(QueryBuilder.delete().from("user_by_email")
				.where(QueryBuilder.eq("email", user.getEmail())));
		batch.add(QueryBuilder.delete().from("user").where(QueryBuilder.eq("id", userId)));
		template.execute(batch);
	}

	@Override
	public void addRole(UUID userId, String role) {
		if (!template.exists(RoleEntity.class, role)) {
			throw new DataIntegrityViolationException("Invalid role: " + role);
		}

		UserRoleEntity userRole = new UserRoleEntity();
		userRole.setUserId(userId);
		userRole.setRole(role);
		template.insert(userRole);
	}

	@Override
	public List<String> findRolesById(UUID userId) {
		Select selectCql = QueryBuilder.select().from("user_role");
		selectCql.where(QueryBuilder.eq("user_id", userId));

		Set<String> allRoles = getRolesSet();
		return template.select(selectCql, UserRoleEntity.class)
			.stream()
				.map(UserRoleEntity::getRole)
				.filter(r -> allRoles.contains(r))
				.collect(Collectors.toList());
	}

	@Override
	public void removeRole(UUID userId, String role) {
		UserRoleEntity userRole = new UserRoleEntity();
		userRole.setUserId(userId);
		userRole.setRole(role);
		template.delete(userRole);
	}

	/**
	 * Checks if email not present in database.
	 * Throws DataIntegrityViolationException if email present database.
	 */
	private void checkEmailNotPresent(String email) {
		if (template.exists(UserByEmailEntity.class, email)) {
			throw new DataIntegrityViolationException("Duplicated user email: " + email);
		}
	}

	/**
	 * Returns set of all roles.
	 */
	private Set<String> getRolesSet() {
		 return roleRepository.findAll().stream().map(Role::getName).collect(Collectors.toSet());
	}
}
