package com.softreactor.template.domain.account.cassandra;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.softreactor.template.domain.account.Role;
import com.softreactor.template.domain.account.RoleRepository;

/**
 * RoleRepositoryImpl class.
 * Implements RoleRepository for Cassandra.
 */
@Component
public class RoleRepositoryImpl implements RoleRepository {

	private final CassandraTemplate template;

	/**
	 * Class constructor.
	 */
	@Autowired
	public RoleRepositoryImpl(CassandraTemplate template) {
		Assert.notNull(template, "CassandraTemplate must not be null");
		this.template = template;
	}

	@Override
	public Role createObject() {
		return new RoleEntity();
	}

	@Override
	public List<? extends Role> findAll() {
		return template.selectAll(RoleEntity.class);
	}

	@Override
	public void save(Role role) {
		if (template.exists(RoleEntity.class, role.getName())) {
			throw new DataIntegrityViolationException("Duplicated role name: " + role.getName());
		}

		template.insert(role);
	}

	@Override
	public void removeByName(String name) {
		template.deleteById(RoleEntity.class, name);
	}

}
