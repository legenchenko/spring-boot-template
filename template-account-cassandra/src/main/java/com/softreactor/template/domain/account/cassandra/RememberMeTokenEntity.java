package com.softreactor.template.domain.account.cassandra;

import java.util.Date;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.softreactor.template.domain.account.RememberMeToken;
import com.softreactor.template.util.DomainUtils;

/**
 * Remember me token entity class for Cassandra.
 */
@Table("remember_me_token")
public class RememberMeTokenEntity implements RememberMeToken {

	@PrimaryKey
	private String id;

	private String username;
	private String token;
	private Date date;

	@Override
	public byte[] getId() {
		return DomainUtils.hexToBytes(id);
	}

	@Override
	public void setId(byte[] id) {
		this.id = DomainUtils.bytesToHex(id);
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public byte[] getToken() {
		return DomainUtils.hexToBytes(token);
	}

	@Override
	public void setToken(byte[] token) {
		this.token = DomainUtils.bytesToHex(token);
	}

	@Override
	public Date getDate() {
		return date;
	}

	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RememberMeTokenEntity other = (RememberMeTokenEntity) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
