package com.softreactor.template.domain.account.cassandra;

import java.util.UUID;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

/**
 * User entity class for Cassandra.
 */
@Table("user")
public class UserEntity extends BaseUserEntity {

	/**
	 * Class constructor.
	 */
	public UserEntity() {
	}

	/**
	 * Constructs object using source object data.
	 */
	public UserEntity(BaseUserEntity source) {
		super(source);
	}

	@PrimaryKey
	@Override
	public UUID getId() {
		return super.getId();
	}

}
