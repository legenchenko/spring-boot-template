package com.softreactor.template.domain.account.jpa;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.softreactor.template.domain.account.User;
import com.softreactor.template.domain.account.UserRepository;

/**
 * User repository JPA interface.
 */
public interface UserRepositoryJpa extends UserRepository, JpaRepository<UserEntity, UUID> {

	default User createObject() {
		return new UserEntity();
	}

	default void save(User user) {
		saveAndFlush((UserEntity) user);
	}

	@Modifying
	@Query(value = Queries.ADD_USER_ROLE, nativeQuery = true)
	void addRole(UUID userId, String trim);

	@Query(Queries.USER_ROLES)
	List<String> findRolesById(UUID userId);

	@Modifying
	@Query(value = Queries.REMOVE_USER_ROLE, nativeQuery = true)
	void removeRole(UUID userId, String role);

}
