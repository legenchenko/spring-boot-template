package com.softreactor.template.domain.account.jpa;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.softreactor.template.domain.account.RememberMeToken;

/**
 * Remember me token entity class for JPA.
 */
@Entity(name = "RememberMeToken")
@Table(indexes = @Index(columnList = "username"))
public class RememberMeTokenEntity implements RememberMeToken {

	@Id
	@Column(columnDefinition = "binary(16)")
	private byte[] id;

	@Column(nullable = false)
	private String username;

	@Column(nullable = false, columnDefinition = "binary(16)")
	private byte[] token;

	@Column(nullable = false, columnDefinition = "timestamp")
	private Date date;

	@Override
	public byte[] getId() {
		return id;
	}

	@Override
	public void setId(byte[] id) {
		this.id = id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public byte[] getToken() {
		return token;
	}

	@Override
	public void setToken(byte[] token) {
		this.token = token;
	}

	@Override
	public Date getDate() {
		return date;
	}

	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + Arrays.hashCode(id);
		result = prime * result + Arrays.hashCode(token);
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RememberMeTokenEntity other = (RememberMeTokenEntity) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (!Arrays.equals(id, other.id))
			return false;
		if (!Arrays.equals(token, other.token))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
