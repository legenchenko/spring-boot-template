package com.softreactor.template.domain.account.jpa;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import com.softreactor.template.domain.account.RememberMeToken;
import com.softreactor.template.domain.account.RememberMeTokenRepository;

/**
 * Remember me token repository JPA interface.
 */
public interface RememberMeTokenRepositoryJpa extends RememberMeTokenRepository,
	JpaRepository<RememberMeTokenEntity, byte[]> {

	default RememberMeToken createObject() {
		return new RememberMeTokenEntity();
	}

	@Modifying
	default void updateToken(byte[] id, byte[] tokenValue, Date lastUsed) {
		RememberMeToken token = findById(id);
		token.setToken(tokenValue);
		token.setDate(lastUsed);
	}

}
